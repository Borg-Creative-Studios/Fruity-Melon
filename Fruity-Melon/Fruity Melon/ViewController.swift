//
//  ViewController.swift
//  Fruity Melon
//
//  Created by Jacob Borg on 4/14/22.
//

import Cocoa
import WebKit
import Foundation

class SFContentBlockerManager : NSObject{}
class SFContentBlockerState : NSObject{
    var isEnabled: Bool { true }
    
}


class ViewController: NSViewController, NSApplicationDelegate, WKNavigationDelegate, WKUIDelegate, NSTextFieldDelegate {
    
    @IBOutlet var window: NSView!
    @IBOutlet weak var forwardButton: NSButton!
    @IBOutlet weak var backButton: NSButton!
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var refreshButton: NSButton!
    
    @IBAction func back(_ sender: Any){
        if webView.canGoBack{
            webView.goBack()
        }
    }
    
    @IBAction func next(_ sender: Any){
        if webView.canGoForward{
            webView.goForward()
        }
    }
    @IBAction func refresh(_ sender: Any){
        
        webView.reload()
    }
    @IBAction func loadHome(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/Project-Pear/pineapple-250b")!))
    }
    
    
    
    
    
    
    @IBAction func ClearOnQuit(_ sender: Any) {
       URLCache.shared.removeAllCachedResponses()
       URLCache.shared.diskCapacity = 0
       URLCache.shared.memoryCapacity = 0
       HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
       print("[WebCacheCleaner] All cookies deleted")
       
       WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
           records.forEach { record in
               WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
               print("[WebCacheCleaner] Record \(record) deleted")
               
           }
           
       }
       NSApplication.shared.terminate(self)
   }
    
    
    @IBAction func clear(_ sender: Any){
       webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/Project-Pear/pineapple-250b")!))
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
                
            }
            
        }
        webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/Project-Pear/pineapple-250b")!))
  
}
    
    
    @IBAction func deleteCookies(_ sender: Any){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
            }
        }
        webView.reload()
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/Project-Pear/pineapple-250b.html")!))
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

